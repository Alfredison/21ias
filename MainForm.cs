﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using PdfiumViewer;

namespace _21IAS
{
    public partial class MainForm : Form
    {
        struct Member
        {
            public string Name { get; set; }     // Имя человека
            public string RefPath { get; set; }  // Путь к реферату
            public string QuizPath { get; set; } // Путь к файлу с вопросами

            public Member(string name, string refPath, string quizPath)
            {
                Name = name;
                RefPath = refPath;
                QuizPath = quizPath;
            }
        }

        List<Member> members;

        private void ParseConfig()
        {
            members = new List<Member>(28);
            var fs = new FileStream("config.txt", FileMode.Open, FileAccess.Read);
            try
            {
                var sr = new StreamReader(fs);
                string cfg;
                while ((cfg = sr.ReadLine()) != null)
                {
                    var tokens = cfg.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                    members.Add(new Member(tokens[0], tokens[1], tokens[2]));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Close();
            }
            finally
            {
                fs.Dispose();
            }
        }

        public MainForm()
        {
            InitializeComponent();
            ParseConfig();

            var renderer = new PdfRenderer();
            renderer.Dock = DockStyle.Fill;
            renderer.ZoomMode = PdfViewerZoomMode.FitWidth;

            foreach (var m in members)
            {
                var tp = new TabPage(m.Name);

                var btn = new Button();
                btn.Text = "Пройти тест";
                btn.Dock = DockStyle.Bottom;
                btn.Height = 30;

                tp.Enter += new EventHandler((sender, e) =>
                {
                    tp.Controls.Add(renderer);
                    try
                    {
                        renderer.Load(PdfDocument.Load(m.RefPath));
                        renderer.Visible = true;
                        renderer.Scroll += (s, ea) =>
                            btn.Visible = renderer.Page > 14;
                        renderer.MouseWheel += (s, ea) =>
                            btn.Visible = renderer.Page > 14;
                    }
                    catch (Exception ex)
                    {
                        renderer.Visible = false;
                        MessageBox.Show(m.Name + ' ' + ex.Message);
                    }
                });

                btn.Click += (sender, e) =>
                {
                    var qf = new QuizForm(m.QuizPath);
                    qf.Show();
                };

                tp.Controls.Add(btn);
                btn.Visible = false;

                memberTabControl.Controls.Add(tp);
            }
        }
    }
}
